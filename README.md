# reign-test-2020

## Installing Docker

You should have docker installed on your operating system. If you don't have a docker installed, you should follow the guide below:

https://docs.docker.com/install/

## Cloning Code

You must download the project code from the following url.

https://gitlab.com/keyfu/reign-test-2020.git

## Running application

After you have installed docker you are ready to run the application.

Open a terminal and go inside the reign-test-2020 folder and verify that a `docker-compose.yml` file exists.

Run the command `docker-compose up`

## Access to application

Open your browse by the next address `localhost:8080` or `localhost:8081` (env prod).

## Locally

Make sure you have mongodb installed.

To run the application locally you must be located in:
    `/reign-test-api/` and run command `npm install`
    `/reign-test-ui/` and run command `npm install`

You must change in app.js this line `const url = config.urlDocker;` for `const url = config.urlLocal;`

Then you should run the following commands:
    `/reign-test-api/` and run command `npm start`
    `/reign-test-ui/` and run command `ng serve`

Open your browse by the next address `localhost:4200`

## Testing

To run the tests locally you must change in app.js this line `const url = config.urlDocker;` for `const url = config.urlLocal;`

Run these command:

`npm install -g istanbul`

Then you should run the command `npm test`

## Note

- The crontab runs from 00:00:00 to 23:00:00 every 1 hour.

- To change the execution times you can modify the tab expression in the file located in: `/reign-test-api/controllers/cron.js`. For example, replace this `0 0 0-23 * * *` for `0 0 0,10,20,30,40,50 * * *` to execute code every 10 minutes.
