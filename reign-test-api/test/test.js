const assert = require('assert');
const ArticleController = require('../controllers/articles');
const ArticleModel = require('../models/articles');

//Require the dev-dependencies
var chai = require('chai'), chaiHttp = require('chai-http');
chai.use(chaiHttp);
let server = require('../app');
let should = chai.should();

const idTest = '010101';

describe('Controller Article', function () {

    before(function () {
        // element test
        element = {
            "_tags": [
                "comment",
                "author_woudsma",
                "story_22648431"
            ],
            "author": "woudsma",
            "comment_text": "I&#x27;m working on a Dokku-like tool for Docker Swarm mode, where you can describe your entire application (stack) in a docker-compose.yml file and simply git push to deploy your stack on a self-hosted server cluster with automatic load-balancing and SSL using Traefik and Let&#x27;s Encrypt.\nThe main idea is to develop locally instead of spending hours on server configuration.<p>I&#x27;m including some nice metrics and examples like Swarmpit, Swarmprom, Grafana, Gitlab CE + Gitlab Runner and documentation using Docusaurus.\nIt works with any amount of server nodes so it&#x27;s easy to start with a single test server and expand as you go.<p>I&#x27;m planning to open-source it ASAP, but things take time.. Hopefully next week v0.1 is done.\nMostly it&#x27;s been a learning experience, finally getting more fluent with bash and learning about distributed systems and networking. I&#x27;ve used Dokku for years but I wanted more flexibility and experience. I&#x27;ve noticed that I learn better when I write about something, so I&#x27;m putting extra effort in writing documentation.<p>It&#x27;s funny that this question comes up, I just thought about it today. We should have #madeinquarantine badges for our repo&#x27;s!",
            "created_at": "2020-03-21T23:00:42.000Z",
            "created_at_i": 1584831642,
            "hidden": false,
            "num_comments": null,
            "objectID": idTest,
            "parent_id": 22648431,
            "points": null,
            "story_id": 22648431,
            "story_text": null,
            "story_title": "Ask HN: What projects are you working on now?",
            "story_url": null,
            "title": null,
            "url": null
        }

        const article = new ArticleModel(element);
        article._id = element.objectID;
        article.hidden = false;

        ArticleController.addArticle(element, article);

    })

    describe('/GET articles', () => {
        it('it should GET all the articles', (done) => {
            chai.request(server)
                .get('/articles')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });

    describe('/DELETE articles ', () => {
        it('should return ERROR when the id article is not found', function (done) {

            chai.request(server)
                .delete('/articles/' + '1')
                .end((err, res) => {
                    res.should.have.status(500);
                    res.body.should.be.a('object');
                    res.body.should.have.property('code');
                    res.body.code.should.equal('ERROR');
                    done();
                });
        });

        it('should return OK when the id article is found', function (done) {

            chai.request(server)
                .delete('/articles/' + idTest)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('code');
                    res.body.code.should.equal('OK');
                    res.body.should.have.property('value');
                    done();
                });
        });
    });

});