const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

var Schema = mongoose.Schema;

var articleSchema = new Schema({
    _id: Number,
    created_at: Date,
    title: String,
    url: String,
    author: String,
    points: Number,
    story_text: String,
    comment_text: String,
    num_comments: Number,
    story_id: Number,
    story_title: String,
    story_url: String,
    parent_id: Number,
    created_at_i: Number,
    _tags: Object,
    objectID: Number,
    hidden: Boolean
});

articleSchema.plugin(mongoosePaginate);

var Article = mongoose.model('Article', articleSchema);

module.exports = Article;