const express = require('express');
const cors = require('cors');
const app = express();
const port = 3000;
const cron = require('./controllers/cron');
var config = require('./settings');

// routes //
const articles = require('./routes/articles');

const mongoose = require('mongoose');

app.use(cors());

const url = config.urlDocker;

mongoose.connect('mongodb://' + url + ':27017/reigndb', { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => {
        console.log('Conexion a la base de datos realizada');
        app.listen(port, () => console.log(`The server is running ${port}!`));
        cron;
    })
    .catch((err) => {
        console.log(err);
    });

app.use('/articles', articles);

module.exports = app;