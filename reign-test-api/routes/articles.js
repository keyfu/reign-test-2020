const express = require('express');
const router = express.Router();
const articlesController = require('../controllers/articles');

// Articles routes
router.get('/', articlesController.getArticles);
router.delete('/:id', articlesController.deleteArticles);

module.exports = router