const cron = require('node-cron');
const axios = require('axios').default;
const ArticleModel = require('../models/articles');
const ArticleController = require('../controllers/articles');

const urlApi = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

cron.schedule('0 0 0-23 * * *', async function () {
    var flag = true;
    var page = 0;
    // console.log(new Date());
    while (flag) {
        await axios.get(urlApi + '&page=' + page.toString())
            .then(res => {
                res.data.hits.forEach(element => {
                    const article = new ArticleModel(element);
                    article._id = element.objectID;
                    article.hidden = false;

                    ArticleController.addArticle(element, article);
                });
                page++;
                // si se llega al final de la paginación se termina de consumir de la api
                if (res.data && res.data.nbPages && page >= res.data.nbPages) flag = false;
            })
            .catch(err => {
                page++;
                console.log(err);
            });
    }
});