const ArticleModel = require('../models/articles');
const filtersH = require('../helpers/filters');

async function addArticle(element, article) {

    ArticleModel.findById( element.objectID, ( err, value ) => {
        if ( !value || !value.hidden ) {
            ArticleModel.updateOne({_id: article._id}, article, { upsert: true, setDefaultsOnInsert: true }, (err, value) => {
                if (err) {
                    return false;
                }
            });
        }                            
    });
    return true;
}

async function getArticles(req, res) {

    const filters = filtersH.buildFilters(req);

    ArticleModel.paginate({ hidden: false }, filters, (err, value) => {
        if (err) {
            return res.status(500).send({
                message: 'ERROR'
            })
        }
        return res.status(200).send({
            value
        })
    });
}

async function deleteArticles(req, res) {
    
    const _id = req.params.id;

    if (_id) {
        await ArticleModel.findOneAndUpdate({ _id: _id }, { hidden: true }, (err, value) => {
            if (err || !value) {
                return res.status(500).send({
                    code: 'ERROR',
                    message: 'Error eliminando Articulo'
                })
            }
            return res.status(200).send({
                code: 'OK',
                message: 'Artículo eliminado satisfactoriamente',
                value
            });
        });
    } else {
        return res.status(500).send({
            message: 'ERROR'
        })
    }    
}

module.exports = {
    addArticle,
    getArticles,
    deleteArticles
}