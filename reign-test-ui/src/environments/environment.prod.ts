export const environment = {
  production: true,
  serverUrl: 'http://localhost:3000/',
  articles: {
    list: 'articles/'
  }
};
