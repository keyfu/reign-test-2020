export interface FiltersModel {
    limit: number;
    page: number;
    sort: string;
    ascending: boolean;
}
