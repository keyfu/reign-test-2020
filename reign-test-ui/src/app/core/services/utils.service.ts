import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() {

  }

  buildFilters(filters) {
    let solve = '';
    Object.keys(filters).forEach((k, idx) => {
      const key = k;
      const value = filters[k];
      if (idx === 0) {
        solve += '?';
      } else {
        solve += '&';
      }
      solve += key + '=' + value;
    });
    return solve;
  }
}
