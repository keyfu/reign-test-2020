import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Article } from '../models/article.model';
import { environment } from '../../../environments/environment';
import { ResponseApi } from '../models/response-api.model';
import { map } from 'rxjs/operators';
import { FiltersModel } from '../models/filters.model';
import { UtilsService } from './utils.service';


@Injectable()
export class ArticlesService {

  serverUrl = environment.serverUrl;
  articlesUrl = environment.articles.list;

  constructor(
    private http: HttpClient,
    private utilsService: UtilsService
  ) { }

  listArticles(filters: FiltersModel) {
    const filtersB = this.utilsService.buildFilters(filters);
    return this.http.get(this.serverUrl + this.articlesUrl + filtersB).pipe(
      map( (resp: any) => {
        return resp.value;
      })
    );
  }

  deleteArticle(id) {
    return this.http.delete(this.serverUrl + this.articlesUrl + id);
  }
}
