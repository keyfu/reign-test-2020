import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { DateCustomPipe } from './pipes/date-custom.pipe';



@NgModule({
  declarations: [DateCustomPipe],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  exports: [
    HttpClientModule,
    DateCustomPipe
  ]
})
export class SharedModule { }
