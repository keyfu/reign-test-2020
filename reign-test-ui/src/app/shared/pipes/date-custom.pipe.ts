import { Pipe, PipeTransform } from '@angular/core';
import { formatDate } from '@angular/common';

const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
  'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
];

@Pipe({
  name: 'dateCustom'
})
export class DateCustomPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {

    // transform
    const date = new Date(value);
    let solve = '';
    const today = new Date();
    const yesterday = new Date(new Date().setDate(today.getDate() - 1));

    if ( today.getDate() === date.getDate() &&
         today.getMonth() === date.getMonth() &&
         today.getFullYear() === date.getFullYear() ) {
        // today
        solve = ' ' + (date.getHours() <= 12 ? date.getHours() : date.getHours() % 12 ) + ':' +
                ( date.getMinutes() < 10 ? '0' : '' ) + date.getMinutes() + ' '
                + ' ' + (date.getHours() / 12 >= 1 ? 'pm' : 'am');
    } else if (
      yesterday.getDate() === date.getDate() &&
      yesterday.getMonth() === date.getMonth() &&
      yesterday.getFullYear() === date.getFullYear() ) {
        // yesterday
        solve = ' ' + 'Yesterday';
    } else {
      solve = ' ' + monthNames[date.getMonth() + 1] + ' ' + date.getDate();
    }

    return solve;
  }

}
