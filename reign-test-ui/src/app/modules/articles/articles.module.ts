import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticlesRoutingModule } from './articles-routing.module';
import { ListComponent } from './components/list/list.component';
import { ArticlesService } from 'src/app/core/services/articles.service';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule, MatIconModule, MatButtonModule, MatSnackBarModule, MatFormFieldModule } from '@angular/material';




@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ArticlesRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    SharedModule
  ],
  providers: [ArticlesService]
})
export class ArticlesModule { }
