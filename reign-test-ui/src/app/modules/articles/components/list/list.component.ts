import { Component, OnInit, ViewChild } from '@angular/core';
import { ArticlesService } from 'src/app/core/services/articles.service';
import { Observable } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import { FiltersModel } from 'src/app/core/models/filters.model';
import { MatSnackBar } from '@angular/material';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  filters: FiltersModel = {
    limit: 10,
    page: 0,
    sort: 'created_at',
    ascending: false
  };

  displayedColumns: string[] = [
    'objectID',
    'created_at',
    'title',
    // 'url',
    'author',
    'points',
    'num_comments',
    'story_id',
    'story_title',
    // 'story_url',
    'actions'
    ];

  articles$: Observable<any>;

  constructor(
    private articlesService: ArticlesService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.listArticles();
  }

  listArticles() {
    this.articles$ = this.articlesService.listArticles(this.filters);
  }

  onChange(event) {
    this.filters.limit = event.pageSize;
    this.filters.page = event.pageIndex;
    this.listArticles();
  }

  onDelete(id) {
    this.articlesService.deleteArticle(id).subscribe( resp => {
      this.snackBar.open('Successfully removed', null, {
        duration: 2000,
        panelClass: ['success-snackbar']
      });
      this.filters.page = 0;
      this.listArticles();
    }, err => {
      this.snackBar.open(err.message, null, {
        duration: 2000,
        panelClass: ['danger-snackbar']
      });
    });
  }

}
